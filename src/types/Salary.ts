import type { Role } from "./Employee"

type Salary = {
    id: number
    name: string
    payDate: Date
    bankAccount: string
    salary: number
    increasesalary: number
    salaryreduction: number
    netpayment: number
    status: string
    roles: Role[]
}
export{ type Salary }