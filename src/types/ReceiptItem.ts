import type { Member } from "./Member";
import type { Product } from "./Product";

type ReceiptItem = {
    id: number;
    name: string;
    member: string | Member;
    price: number;
    unit: number;
    paymentType: string;
    productsId: number;
    products?: Product;
}
export { type ReceiptItem }