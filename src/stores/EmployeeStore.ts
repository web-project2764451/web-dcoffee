// import {type  Employee } from "@/types/Employee";
import { computed, nextTick, onMounted } from 'vue'
import { ref } from 'vue'
import type { VForm } from 'vuetify/components'
import  type { Employee } from '@/types/Employee'
import { defineStore } from 'pinia'


export const useEmployee = defineStore('employees', () =>{

  const initialEmployee: Employee = {
    id: -1,
    name: '',
    email: '',
    tel: '',
    password: '',
    gender: 'male',
    roles: ['manager']
  }
  const search = ref('')
  const filteredEmployee = computed(() => {
    return employees.value.filter((employees) => employees.name.includes(search.value))
  })
  
  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initialEmployee)))
  const employees = ref<Employee[]>([])
  let editedIndex = -1
  let lastId = 6
  const itemsPerPage = ref<any>(5)
  const page = ref(1)
  const pageCount = computed(() => {
    return Math.ceil(employees.value.length / itemsPerPage.value)
  })
  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)
  
  const dialogDelete = ref(false)

  const loading = ref(false)

  const headers = [
    {
      title: 'ID',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Email',
      key: 'email',
      value: 'email'
    },
    {
      title: 'Tel',
      key: 'tel',
      value: 'tel'
    },
    {
      title: 'Gender',
      key: 'gender',
      value: 'gender'
    },
    {
      title: 'Roles',
      key: 'roles',
      value: (item: any | Employee) => item.roles.join(', ')
    },
    { title: '', key: 'actions', sortable: false }
  ]
  function initialize() {
    employees.value = [
      {
        id: 1,
        name: 'manager',
        email: 'managerMail',
        tel: '0987654321',
        password: 'pass@word123',
        gender: 'female',
        roles: ['manager', 'staff']
      },
      {
        id: 2,
        name: 'kanoknapas',
        email: 'managerMail2',
        tel: '0987653466',
        password: 'pass@word123',
        gender: 'male',
        roles: ['manager', 'staff']
      },
      {
        id: 3,
        name: 'staff',
        email: 'staffMail',
        tel: '0987634876',
        password: 'pass@word123',
        gender: 'female',
        roles: ['staff']
      },
      {
        id: 4,
        name: 'top',
        email: 'staffMail2',
        tel: '0987947455',
        password: 'pass@word123',
        gender: 'male',
        roles: ['staff']
      },
      {
        id: 5,
        name: 'staff3',
        email: 'staffMail3',
        tel: '0987653466',
        password: 'pass@word123',
        gender: 'female',
        roles: ['staff']
      }
    ]
  }
  onMounted(() => {
    initialize()
  })
  
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedEmployee.value = Object.assign({}, initialEmployee)
      editedIndex = -1
    })
  }
  
  async function save() {
    const { valid } = await refForm.value!.validate()
  
    if (!valid) return
    if (editedIndex > -1) {
      Object.assign(employees.value[editedIndex], editedEmployee.value)
    } else {
      editedEmployee.value.id = lastId++
      employees.value.push(editedEmployee.value)
    }
    closeDialog()
  }

    
    
  
  
  function onSubmit() {}
  
  function editItem(item: Employee) {
    editedIndex = employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    dialog.value = true
  }
  
  function deleteItem(item: Employee) {
    editedIndex = employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    dialogDelete.value = true
    
  }
  
  function deleteItemConfirm() {
    employees.value.splice(editedIndex, 1)
    closeDelete()
    editedIndex = -1
  }
  
  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedEmployee.value = Object.assign({}, initialEmployee)
    })
  }
    return { form, loading, itemsPerPage, page, pageCount, filteredEmployee, headers, save, onSubmit, editItem, deleteItem, deleteItemConfirm, editedEmployee, dialog, search, closeDialog, dialogDelete, closeDelete, initialize}
})